// Project 1 - Rob Johnson - PCC cs162 - 09/26/2018

#include <iostream>
#include <cstring>
#include <ctime>
#include <cstdlib>

#define NUM_GUESSES 3

using namespace std;

int main()
{
    char name[128];             // cstring. Player's name
    char playAgain;
    const char ordinal[NUM_GUESSES][10] = {"first", "second", "third"};
    int pot = 100;              // player's gambling tokens
    int bet;                    // amount of tokens the player is betting
    int guess[NUM_GUESSES];     // numbers that the player guesses
    int lottery[NUM_GUESSES];   // numbers that the player is trying to guess
    int matches;                // number of correct guesses

    srand((unsigned)time(0));   // seeds the prng

    cout << "&&&&&&& LOTTERY GAME &&&&&&&&&&&" << endl << endl;

    cout << "Please enter your name: ";
    cin >> name;

    cout << "Hello " << name << endl << endl;

    do {
        // generate the random values to be guessed
        for(int i = 0; i < NUM_GUESSES; ++i)
        {
            lottery[i] = (rand() % 6) + 1;
        }

        cout << "You have " << pot << " coins." << endl;
        cout << "Well, " << name << " please enter a bet amount (between 0 and " << pot << " coins): ";
        cin >> bet;
        while(cin.fail() || bet < 0 || bet > pot)
        {
            cin.clear();
            cin.ignore(100, '\n');
            cout << "Invalid entry. Please enter a bet amount (between 0 and " << pot << " coins): ";
            cin >> bet;
        }

        for(int i = 0; i < NUM_GUESSES; ++i)
        {
            cout << "Now enter the " << ordinal[i] << " number (between 1 and 6): ";
            cin >> guess[i];
            while(cin.fail() || guess[i] < 1 || guess[i] > 6)
            {
                cin.clear();
                cin.ignore(100, '\n');
                cout << "Invalid entry. Please enter the " << ordinal[i] << " number (between 1 and 6): ";
                cin >> guess[i];
            }
        }

        cout << "The lottery numbers are: ";
        for(int i = 0; i < NUM_GUESSES; ++i)
        {
            cout << lottery[i] << " ";
        }
        cout << endl;

        // calculate number of matches
        matches = 0;
        for(int i = 0; i < NUM_GUESSES; ++i)
        {
            if(guess[i] == lottery[i])
            {
                matches += 1;
            }
        }

        if(matches == 0)
        {
            cout << name << ", you didn't guess any numbers :-(" << endl;
            cout << "you lose " << bet << " coins" << endl;
            pot -= bet;
        }
        else
        {
            cout << name << ", you guessed " << matches << " number(s)! :-)" << endl;
            cout << "you get " << matches * bet << " added to your pot." << endl;
            pot += matches * bet;
        }

        if(pot <= 0)
        {
            cout << endl << name << ", you have run out of money!" << endl << endl;
            break;
        }

        cout << name << ", play again (y/n)? ";
        cin >> playAgain;
        while(cin.fail() || (playAgain != 'y' && playAgain != 'n'))
        {
            cin.clear();
            cin.ignore(100, '\n');
            cout << "Invalid entry. Play again (y/n)? ";
            cin >> playAgain;
        }
        cout << endl;
    } while (playAgain == 'y');

    cout << "========> " << name << " pot is: " << pot << " coins." << endl;
    cout << "Goodbye!" << endl;
    cout << endl << "===================================" << endl;

    return 0;
}
